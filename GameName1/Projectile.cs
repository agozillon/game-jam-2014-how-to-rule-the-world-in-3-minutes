﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using RenderHelpers;
using Player;

namespace Projectile
{
    class Projectiles
    {
        //public AnimatedSprite push;

        public int lifeTime;
        public int cooldown;

        public bool pushed = false;
        public bool recharging = false;

        public Projectiles(/*ContentManager content, string[] fileName, int xpos, int ypos, int ID*/)
        {
            this.lifeTime = 5000;
            this.cooldown = 5000;
        }


        public void playerCollider(ref Players[] player, int id, int numberOfPlayers, TimeSpan time, bool fired)
        {

            int playerid = id;

            if (pushed)
                recharging = true;

            if (!recharging)
            {
                for (int i = 0; i < numberOfPlayers; i++)
                {
                    if (i != id)
                    {
                        if (player[playerid].Character.getBbox().Intersects(player[i].Character.getBbox()) && i != playerid)
                        {
                            pushed = true;

                            if (player[playerid].Character.getTextureObject().rect.X < player[i].Character.getTextureObject().rect.X)
                            {
                                player[i].pushForce = 15;
                            }
                            else if (player[playerid].Character.getTextureObject().rect.X > player[i].Character.getTextureObject().rect.X)
                            {
                                player[i].pushForce = -15;
                            }
                        }
                    }
                }
            }

            if (cooldown > 0 && pushed)
            {
                cooldown -= Convert.ToInt32(time.Milliseconds);

            }
            else
            {
                cooldown = 3000;
                pushed = false;
                recharging = false;
            }
        }


    }
}
