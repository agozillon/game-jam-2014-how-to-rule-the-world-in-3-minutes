﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace InputControl
{
    public class InputControls
    {
        GamePadState[] pad;
        int noPads = 0;
        KeyboardState keyboard = new KeyboardState();
        PlayerIndex[] playerArray;

        MouseState mouse = new MouseState();
        int x, y;

        public InputControls()
        {
        }

        public void init()
        {
            for (PlayerIndex i = PlayerIndex.One; i <= PlayerIndex.Four; i++)
            {
                GamePadState state = GamePad.GetState(i);
                if (state.IsConnected)
                    noPads++;
            }
            Console.WriteLine("number of controllers detected");
            Console.WriteLine(noPads);

            pad = new GamePadState[noPads]; // creates game pads for selected number of players

            playerArray = new PlayerIndex[noPads];

            int j = 0;
            for (PlayerIndex i = PlayerIndex.One; i <= PlayerIndex.Four; i++)
            {
                if ( GamePad.GetState(i).IsConnected )
                    playerArray[j] = i;
                j++;
            }

        }

        public int getNoPads()
        {
            return noPads;
        }

        public bool buttonDown(int controller, Buttons button)
        {
            return pad[controller].IsButtonDown(button);
        }

        public bool buttonUp(int controller, Buttons button)
        {
            return pad[controller].IsButtonUp(button);
        }

        public float getAxis(int controller, string axis)
        {
            float returnInt = 0;

            if (axis == "leftX")
                returnInt = pad[controller].ThumbSticks.Left.X;
            else if (axis == "leftY")
                returnInt = pad[controller].ThumbSticks.Left.Y;
            else if (axis == "rightX")
                returnInt = pad[controller].ThumbSticks.Right.X;
            else if (axis == "rightY")
                returnInt = pad[controller].ThumbSticks.Right.Y;
            else if (axis == "leftTrigger")
                returnInt = pad[controller].Triggers.Left;
            else if (axis == "rightTrigger")
                returnInt = pad[controller].Triggers.Right;
            else
                returnInt = 0;

            return returnInt;
        }

         public bool getKey(Keys key)
        {
            return keyboard.IsKeyDown(key);
        }

         public bool mouseLeftClick()
         {
             return mouse.LeftButton == ButtonState.Pressed;
         }

         public int getMouseX() { return x; }
         public int getMouseY() { return y; }

         public bool mouseOverButton(int buttonX, int buttonY, int buttonW, int buttonH)
         {
             bool status = false;

             if (x > buttonX && x < buttonX + buttonW)
             {
                 if (y > buttonY && y < buttonY + buttonH)
                     status = true;
             }
             else status = false;

             return status;
         }

        public void checkInput()
        {
            mouse = Mouse.GetState();
            x = mouse.X;
            y = mouse.Y;
            keyboard = Keyboard.GetState();

            for (int i = 0; i < noPads; i++)
            {
                pad[i] = GamePad.GetState(playerArray[i]);
            }
        }

    }			
}



