﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using Player;
using RenderHelpers;


namespace Powerups
{
    class PowerUps
    {
        //private int powerUpID;
        private int currentPowerUp;
        private int xPos;
        private int yPos;

        public int slowIdentifier = 1;
        public int freezeIdentifier = 2;
        public static Random powerup = new Random();
        public int power = powerup.Next(DateTime.Now.Second * 1000) % 2 + 1;
        public TextureObject powerUp;

        public PowerUps()
        {
            currentPowerUp = power;
        }

        public void RandomLocationSpawner(ContentManager content/*, string fileName*/)
        {
            string fileName;
            Random xVar = new Random();
            int xpos = xVar.Next(DateTime.Now.Second* 1000) % 1180 + 50;
            Random yVar = new Random();
            int ypos = yVar.Next(DateTime.Now.Second * 1000) % 621 + 50;
            power = powerup.Next(DateTime.Now.Second * 1000) % 2 + 1;
            currentPowerUp = power;
            if (power == 1)
                fileName = "slow_pickup_icon.png";
            else
                fileName = "ice_pickup_icon.png";
            powerUp = new TextureObject(content, fileName, xpos, ypos, 25, 25);
            xPos = xpos;
            yPos = ypos;
        }

       // public void RandomTimeSpawner()
       // {
        
        //}

        public void pickUpPowerUp(Players player)
        {
            player.powerUp = currentPowerUp;
        }

        public void update()
        {
            powerUp.updatePosition(xPos, yPos);
            powerUp.updateBounding();
        }

        public void DrawPowerUp(ref SpriteBatch spriteBatcher)
        {
            powerUp.drawMe(ref spriteBatcher);
        }

        //current power up values.
        //atm - 1 is slow
        //2 is freeze
        //public void PlayerPowerUps(int powerUp, Players player)
        //{
        //    currentPowerUp = powerUp;
        //    if(currentPowerUp == 1 || currentPowerUp == 2)
        //    {
        //        player.moveSpeedModifiers(currentPowerUp);
        //    }
        //}

        public void changeXandY(int xpos, int ypos)
        {
            xPos = xpos;
            yPos = ypos;
        }

    }
}
