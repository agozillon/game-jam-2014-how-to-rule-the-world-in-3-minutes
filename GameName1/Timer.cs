﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using RenderHelpers;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;


// only works with seconds at the moment, most common use...
namespace Time
{
    class Timer
    {
        int currentTime;
        int totalTime;
        bool timerUp;
        string textDisplay;
        TextSprite timeDisplay;
        Stopwatch time;

        public Timer()
        {
           
        }

        public void init(int seconds)
        {
            time = new Stopwatch();
            totalTime = seconds;
            currentTime = 0;
            timerUp = false;
            time.Start();
        }

        public void initDisplay(ContentManager content, string fontName, string text, Vector2 position)
        {
            textDisplay = text;
            timeDisplay = new TextSprite(fontName, content, text + currentTime, position, Color.White);
        }

        public void updateTextDisplay(string nText)
        {
            textDisplay = nText;
            timeDisplay.updateTextDisplay(nText);
        }


        public void update()
        { 
            currentTime = totalTime - Convert.ToInt32(time.Elapsed.TotalSeconds);
           
            if (currentTime <= 0)
            {
                timerUp = true;
            }

        }

        public bool getTimer()
        {
            return timerUp;
        }

        public void setTimer(int newStartTime)
        {
            timerUp = false;
            totalTime = newStartTime;
            currentTime = 0;
            time.Restart();
        }

        public void drawMe(ref SpriteBatch spriteBatcher)
        {
            timeDisplay.updateTextDisplay(textDisplay + currentTime);
            timeDisplay.drawMe(ref spriteBatcher);
        }

    }

}
