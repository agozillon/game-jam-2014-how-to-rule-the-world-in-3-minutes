﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using InputControl;


namespace Screens
{
    class ScreenManager
    {
        ScreenAbstract currentScreen;
        ScreenAbstract menuScreen = new ScreenMenu();
        ScreenAbstract gameScreen = new ScreenGame();
        ScreenAbstract creditsScreen = new ScreenCredits();

        ContentManager content;
        SpriteBatch spriteBatch;
        InputControls inputControl;

        SoundEffect music;
        SoundEffectInstance musicLoop;
        SoundEffect intro;
        SoundEffectInstance introLoop;

        public ScreenManager()
        {
            currentScreen = menuScreen;
        }

        public void init(ContentManager iContent, SpriteBatch iSpriteBatch)
        {
            content = iContent;
            spriteBatch = iSpriteBatch;
            inputControl = new InputControls();

            inputControl.init();
            currentScreen.init(content, spriteBatch, inputControl);

            music = content.Load<SoundEffect>("musicedited.wav");
            intro = content.Load<SoundEffect>("introloop.wav");

            musicLoop = music.CreateInstance();
            introLoop = intro.CreateInstance();

            musicLoop.IsLooped = true;
            introLoop.IsLooped = true;

            introLoop.Play();    
        }

        public void draw() 
        {
            currentScreen.draw();
        }

        public void update()
        {
            currentScreen.update(this);
            inputControl.checkInput();
        }

        public void setCurrentScreen(int screenID)
        {
            if (screenID == 0)
            {
                musicLoop.Stop();
                currentScreen = menuScreen;
                introLoop.Play();
            }
            else if (screenID == 1)
            {
                introLoop.Stop();
                currentScreen = gameScreen;
                musicLoop.Play();
            }
            else if (screenID == 2)
                currentScreen = creditsScreen;

            currentScreen.init(content, spriteBatch, inputControl);
        }
    }
}
